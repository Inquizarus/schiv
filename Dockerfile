FROM golang:alpine as builder

RUN apk update && apk add ca-certificates

COPY . $GOPATH/src/gitlab.com/inquizarus/schiv/

WORKDIR $GOPATH/src/gitlab.com/inquizarus/schiv 
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o /go/bin/schiv

FROM busybox:latest
COPY --from=builder /go/bin/schiv /go/bin/schiv
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

WORKDIR /go/bin
ENTRYPOINT ["./schiv"]