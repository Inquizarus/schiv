package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/inquizarus/schiv/pkg/util"
)

func TestThatItCanFetchExternalIPAndWriteIt(t *testing.T) {

	expectedIP := "31.15.51.116"

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf(`{"ip":"%s"}`, expectedIP)))
	}))

	client := &util.StandardClient{
		Base: server.URL,
	}

	// Set up whatever we want to use as validation later
	writer := bytes.NewBuffer([]byte{})

	ipCmd := makeIPCmd(writer, client, func(err error) {
		if nil != err {
			t.Errorf("error occured during request, %s", err)
		}
	})

	ipCmd.Execute()
	result, _ := ioutil.ReadAll(writer)
	receivedIP := strings.Trim(string(result), "\n")
	if expectedIP != receivedIP {
		t.Errorf("%v did not match the expected result of %s", receivedIP, expectedIP)
	}

}
