// Copyright © 2018 Conny Karlsson <connykarlsson9@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/shirou/gopsutil/disk"
	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/schiv/internal/cmdutil"
	"gitlab.com/inquizarus/schiv/pkg/messages"
	"gitlab.com/inquizarus/schiv/pkg/util"
)

func makeDiskCmd(eh util.ErrorHandler) *cobra.Command {
	return &cobra.Command{
		Use:   "dsk",
		Short: "report information about system disk",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			parts, err := disk.Partitions(false)
			eh(err)

			dustr := ""

			for _, part := range parts {
				u, err := disk.Usage(part.Mountpoint)
				eh(err)
				if false == strings.Contains(u.Path, "/snap/") {
					dustr = dustr + u.Path + "\t" + strconv.FormatFloat(u.UsedPercent, 'f', 2, 64) + "% full.\n"
					dustr = dustr + "Total: " + strconv.FormatUint(u.Total/1024/1024/1024, 10) + " GiB\n"
					dustr = dustr + "Free:  " + strconv.FormatUint(u.Free/1024/1024/1024, 10) + " GiB\n"
					dustr = dustr + "Used:  " + strconv.FormatUint(u.Used/1024/1024/1024, 10) + " GiB\n"
				}
			}
			dispatcher, err := cmdutil.DetermineAndBuildDispatcher(cmd)
			eh(err)
			hostname, err := os.Hostname()
			eh(err)
			msg := messages.DispatcherMessage{Subject: fmt.Sprintf("Disk usage on %s", hostname), Content: dustr}
			eh(dispatcher.Send(msg))
		},
	}
}
