// Copyright © 2018 Conny Karlsson <connykarlsson9@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/schiv/internal/cmdutil"
	"gitlab.com/inquizarus/schiv/pkg/commands"
	"gitlab.com/inquizarus/schiv/pkg/util"
)

func makeIPCmd(c util.HttpClient, eh util.ErrorHandler) *cobra.Command {
	return &cobra.Command{
		Use:   "ip",
		Short: "retrieves public ip",
		Long:  `retrieves and emits this machines public ip by using external service providers`,
		Run: func(cmd *cobra.Command, args []string) {
			dispatcher, err := cmdutil.DetermineAndBuildDispatcher(cmd)
			eh(err)
			commands.Ip(c, dispatcher, eh)
		},
	}
}
