package cmd

import (
	"fmt"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/schiv/internal/cmdutil"
	"gitlab.com/inquizarus/schiv/pkg/util"
)

var rootCmd *cobra.Command
var forever = false

const configType = "json"
const appName = "schiv"

// Execute schiv
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func makeRootCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "schiv",
		Short: "schiv is a utility application that can emit changes to a desired location",
		Long:  ``,
	}
}

func init() {
	if nil == rootCmd {
		rootCmd = makeRootCmd()
	}
	cobra.OnInitialize(initConfig)
	cmdutil.ConfigurePersistentFlags(rootCmd)
	errorHandler := util.MakeExitErrorHandler(os.Stdout)
	rootCmd.AddCommand(
		makeIPCmd(
			&util.StandardClient{Base: "https://api.ipify.org"},
			errorHandler,
		),
		makeSysCmd(
			errorHandler,
		),
		makeDockerCmd(
			errorHandler,
		),
	)

}

func initConfig() {
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/appname/")
	viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", appName))
	viper.AddConfigPath(".")
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	viper.SetConfigType(configType)
	viper.AutomaticEnv()
	viper.SetEnvPrefix(appName)
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
}
