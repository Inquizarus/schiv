package main

import (
	"gitlab.com/inquizarus/schiv/cmd"
)

func main() {
	cmd.Execute()
}
