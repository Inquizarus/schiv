package cmdutil

import (
	"github.com/spf13/cobra"
)

// ReportTo represents the "driver" to where reports will be sent
var ReportTo string

// ReportTarget used by ReportTo to determine where driver will send the report
var ReportTarget string

// RunForever makes the process run forever with continous executions of the command
var RunForever bool

// ConfigurePersistentFlags configures all the persistent flags on passed command
func ConfigurePersistentFlags(command *cobra.Command) {
	command.PersistentFlags().StringVar(&ReportTo, "report-to", "stdout", "driver to use for reporting")
	command.PersistentFlags().StringVar(&ReportTarget, "report-target", "", "target for the driver")
	command.PersistentFlags().BoolVar(&RunForever, "run-forever", false, "makes the command run forever with calls every one second")
}
