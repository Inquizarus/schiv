package cmdutil

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/schiv/pkg/formatting"
	"gitlab.com/inquizarus/schiv/pkg/report"
	"gitlab.com/inquizarus/schiv/pkg/writer"
)

// DetermineAndBuildDispatcher checks flags in cmd and creates a report dispatcher based on that
func DetermineAndBuildDispatcher(cmd *cobra.Command) (report.Dispatcher, error) {
	var err error
	v, err := cmd.Flags().GetString("report-to")
	switch v {
	case "slack":
		dispatcher, err := BuildSlackDispatcher(cmd)
		return dispatcher, err
	default:
		return report.NewWriterDispatcher(os.Stdout, nil), err
	}
}

// BuildSlackDispatcher configures based on cmd and returns a reporter that posts to slack
func BuildSlackDispatcher(cmd *cobra.Command) (report.Dispatcher, error) {
	slackURL, err := cmd.Flags().GetString("report-target")
	if nil != err || slackURL == "" {
		return report.NewWriterDispatcher(writer.NewNullWriter(), nil), fmt.Errorf("--report-target must be used in conjuction with --report-to=slack, ")
	}

	return report.NewWriterDispatcher(writer.NewSlackWriter(slackURL), &formatting.SlackMessageFormatter{}), nil
}
