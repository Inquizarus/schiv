package model

// IpifyDTO represents the response from ipify api
type IpifyDTO struct {
	IP string `json:"ip"`
}
