package formatting

import "gitlab.com/inquizarus/schiv/pkg/messages"

// MessageFormatter is an interface to massage message content into something desired for a given target
type MessageFormatter interface {
	Format(messages.DispatcherMessage) (string, error)
}
