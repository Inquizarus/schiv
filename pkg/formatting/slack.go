package formatting

import (
	"fmt"

	"gitlab.com/inquizarus/schiv/pkg/messages"
)

// SlackMessageFormatter implements message formatter interface for slack
type SlackMessageFormatter struct{}

// Format implementation for SlackMessageFormatter
func (smf *SlackMessageFormatter) Format(msg messages.DispatcherMessage) (string, error) {
	format := "*%s*\n%s"
	return fmt.Sprintf(format, msg.Subject, msg.Content), nil
}
