package report

import (
	"fmt"
	"io"

	"gitlab.com/inquizarus/schiv/pkg/formatting"
	"gitlab.com/inquizarus/schiv/pkg/messages"
)

// Dispatcher interface for where schiv output its report
type Dispatcher interface {
	Send(messages.DispatcherMessage) error
	formatter() formatting.MessageFormatter
}

// WriterDispatcher implentation of Dispatcher where send report is written to an io writer
type WriterDispatcher struct {
	w io.Writer
	f formatting.MessageFormatter
}

// Send implementation for WriterDispatcher
func (wd *WriterDispatcher) Send(msg messages.DispatcherMessage) error {
	var content string
	var err error

	if nil != wd.f {
		content, err = wd.f.Format(msg)
	}
	if nil != err {
		return err
	}
	if "" == content {
		content = msg.Content
	}
	_, err = fmt.Fprint(wd.w, content)
	return err
}

func (wd *WriterDispatcher) formatter() formatting.MessageFormatter {
	return wd.f
}

// NewWriterDispatcher builds and returns a WriterDispatcher
func NewWriterDispatcher(w io.Writer, f formatting.MessageFormatter) Dispatcher {
	return &WriterDispatcher{
		w: w,
		f: f,
	}
}
