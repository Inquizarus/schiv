package util

import (
	"fmt"
	"io"
	"os"
)

// ErrorHandler is a function that encapsulates error handling
type ErrorHandler func(error)

// MakeExitErrorHandler pulls the handbreak by writing error to passed writer and then exit with status 1
func MakeExitErrorHandler(w io.Writer) ErrorHandler {
	return func(err error) {
		if nil != err {
			fmt.Fprint(w, err)
			os.Exit(1)
		}
	}
}
