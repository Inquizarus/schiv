package util

import (
	"fmt"
	"net/http"
)

type HttpClient interface {
	Get(string) (*http.Response, error)
}

type StandardClient struct {
	Base string
}

func (c *StandardClient) Get(u string) (*http.Response, error) {
	return http.DefaultClient.Get(fmt.Sprintf("%s%s", c.Base, u))
}
