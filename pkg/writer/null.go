package writer

import (
	"fmt"
	"io"
	"os"
)

// NullWriter is an implementation for sending strings to slack
type NullWriter struct{}

func (nw *NullWriter) Write(p []byte) (n int, err error) {
	fmt.Fprintf(os.Stdout, "The null writer consumes \"%s\"", p)
	return len(p), err
}

// NewNullWriter builds and returns a NullWriter
func NewNullWriter() io.Writer {
	return &NullWriter{}
}
