package writer

import (
	"bytes"
	"fmt"
	"io"
	"net/http"

	"github.com/pkg/errors"
)

// SlackWriter is an implementation for sending strings to slack
type SlackWriter struct {
	webhookURL string
}

func (sw *SlackWriter) Write(p []byte) (n int, err error) {
	msg := fmt.Sprintf(`{"text":"%s", "markdown": true}`, p)
	msgb := []byte(msg)
	response, err := http.Post(sw.webhookURL, "application/json", bytes.NewReader(msgb))
	if nil != err {
		return 0, errors.Wrap(err, "could not write to slack webhook endpoint url")
	}
	if response.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("could not write to slack webhook endpoint url, got bad http status code %d", response.StatusCode)
	}
	return len(p), err
}

// NewSlackWriter builds and returns a SlackWriter
func NewSlackWriter(webhookURL string) io.Writer {
	return &SlackWriter{
		webhookURL: webhookURL,
	}
}
