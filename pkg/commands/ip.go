package commands

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/inquizarus/schiv/pkg/report"

	"gitlab.com/inquizarus/schiv/pkg/messages"
	"gitlab.com/inquizarus/schiv/pkg/model"
	"gitlab.com/inquizarus/schiv/pkg/util"
)

func Ip(c util.HttpClient, d report.Dispatcher, eh util.ErrorHandler) {
	var dto model.IpifyDTO
	resp, err := c.Get("?format=json")
	eh(err)
	defer resp.Body.Close()
	bbytes, err := ioutil.ReadAll(resp.Body)
	eh(err)
	eh(json.Unmarshal(bbytes, &dto))
	hostname, err := os.Hostname()
	eh(err)
	msg := messages.DispatcherMessage{Subject: fmt.Sprintf(`external ip for %s`, hostname), Content: dto.IP}
	eh(d.Send(msg))
}
