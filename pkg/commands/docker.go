package commands

import (
	"fmt"

	d "github.com/shirou/gopsutil/docker"
)

func Docker() {
	dls, err := d.GetDockerIDList()
	fmt.Println(dls, err)
}
