package messages

// DispatcherMessage is a container for whatever will be sent through a Dispatcher
type DispatcherMessage struct {
	Subject string
	Content string
}
