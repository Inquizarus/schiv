# SCHIV

Small package that aims to emit some valuable information to desired target. The idea is to
add the possibility to have it run in the background and keep track on if something changes and send the
new information to wherever you want by a given time interval.

Currently only displays your external IP to stdout.

Run with
```
$ docker run --rm registry.gitlab.com/inquizarus/schiv:latest
```